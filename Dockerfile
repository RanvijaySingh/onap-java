FROM openjdk:11.0.5-jre-slim

LABEL maintainer="ONAP Integration team, morgan.richomme@orange.com"
LABEL Description="Reference ONAP JAVA 11 image"

ENV JAVA_OPTS="-Xms256m -Xmx1g"
ENV JAVA_SEC_OPTS=""

ARG user=onap
ARG group=onap

# Create a group and user
RUN groupadd -r $group && useradd -ms /bin/bash $user -g $group && \
    mkdir /opt/$user && \
    chown -R $user:$group /opt/$user &&  \
    mkdir /var/log/$user && \
    chown -R $user:$group /var/log/$user


# Tell docker that all future commands should be run as the onap user
USER $user
WORKDIR /opt/$user

ENTRYPOINT exec java $JAVA_SEC_OPTS $JAVA_OPTS -jar /opt/$user/app.jar
