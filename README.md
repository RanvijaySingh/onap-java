# onap-java

This project aims to produce a reference ONAP baseline including JAVA 11.
It is based on the image 11.0.5-jre-slim.

When you create your docker from this docker you need to copy your jar file
under /opt/onap/app.jar.

You may sepecif 2 env variable to customize the way you are stating java:

- ENV JAVA_OPTS: by default set to -Xms256m -Xmx1g
- ENV JAVA_SEC_OPTS: empty by default
